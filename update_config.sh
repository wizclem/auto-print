#!/bin/bash
arg1=$1
arg2=$2

source `dirname $0`"/includes.sh" || exit 99



touch $custom_config



if [ "$arg2" == "debug" ]
then
    egrep "^print_debug=" $custom_config > /dev/null || echo "print_debug=false" >> $custom_config
     [ "$arg1" == "enable" ] && sed -i 's,^print_debug=.*,print_debug=true,g' $custom_config
     [ "$arg1" == "disable" ] && sed -i 's,^print_debug=.*,print_debug=false,g' $custom_config
     egrep "^print_debug=" $custom_config
fi
