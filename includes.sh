################################################
# load configs
################################################
full_path=`realpath $0`
base_dir=`dirname $full_path`
default_config=$base_dir/config.default.sh
custom_config=$base_dir/config.custom.sh
source $default_config || exit 21
[ -f $custom_config ] && source $custom_config



################################################
# prerequisite functions
################################################
my_pid=$$
function echo_debug {
    $print_debug || return 0
    msg=$1
    echo "[$my_pid] [$(date -Isecond)] [debug] - $msg" 1>&2
}
function echo_info {
    msg=$1
    echo "[$my_pid] [$(date -Isecond)] [info] - $msg" 1>&2
}
function echo_warning {
    msg=$1
    echo "[$my_pid] [$(date -Isecond)] [WARNING] - $msg" 1>&2
}
function echo_error {
    msg=$1
    echo "[$my_pid] [$(date -Isecond)] [ERROR] - $msg" 1>&2
}
function elem_in_list {
    element=$1
    list=$2
    echo "$list" | grep --color "$element" > /dev/null; grep_exit_code=$?
    if [ $grep_exit_code -eq 0 ]
    then
        #echo_debug "element [$element] IS in list [$list]"
        return 0
    else
        #echo_debug "element [$element] is NOT in list [$list]"
        return 1
    fi
}



################################################
# global variables
################################################
script_params=$@
#echo_debug "script_params[$script_params]"
elem_in_list "onerun" "$script_params" && onerun=true || onerun=false
$onerun && echo_info "== ONE RUN mode =="
#echo_debug "base_dir[$base_dir]"
logs_dir=$base_dir/logs
#echo_debug "logs_dir[$logs_dir]"
data_dir=$base_dir/data
#echo_debug "data_dir[$data_dir]"
script_lock=$data_dir/script.lock
timestamp=`date +%s`
my_id=$my_pid"_"$timestamp
#echo_debug "script_lock[$script_lock] my_id[$my_id]"



################################################
# locks
################################################
function clean_expired_script_lock {
    count=`find $data_dir -type f -name "script.lock" -mmin +$script_lock_expiration_minutes | wc -l`
    [ $count -eq 1 ] && rm -v $script_lock
}
function take_script_lock_or_exit {
    clean_expired_script_lock
    if [ -f $script_lock ]
    then
        echo_debug "script lock already taken. Exiting."
        exit 22
    fi
    echo $my_id > $script_lock
    sleep 2
    egrep --color "^$my_id\$" $script_lock > /dev/null; egrep_exit_code=$?
    if [ $egrep_exit_code -ne 0 ]
    then
        echo_debug "An other instance took the lock. Exiting."
        exit 23
    fi
}
function refresh_script_lock_or_exit {
    echo_debug "refreshing script lock..."
    egrep --color "^$my_id\$" $script_lock > /dev/null; egrep_exit_code=$?
    if [ $egrep_exit_code -ne 0 ]
    then
        echo_warning "Could not refresh lock O.o Exiting."
        exit 24
    fi
    touch $script_lock
}
function release_script_lock {
    echo_debug "releasing script lock..."
    egrep --color "^$my_id\$" $script_lock > /dev/null; egrep_exit_code=$?
    if [ $egrep_exit_code -ne 0 ]
    then
        echo_warning "Lock taken by an other instance O.o"
        return 1
    fi
    rm -v $script_lock
}
function wait_for_script_lock_release {
    timeout_sec=$1
    start_wait=$(date +%s)
    while [ $(date +%s) -lt $(($start_wait + $timeout_sec)) ]
    do
        echo -n "r"
        clean_expired_script_lock
        [ -f $script_lock ] || return 0
        sleep 0.2
    done
    return 1
}
function wait_for_script_lock {
    timeout_sec=$1
    start_wait=$(date +%s)
    while [ $(date +%s) -lt $(($start_wait + $timeout_sec)) ]
    do
        echo -n "l"
        [ -f $script_lock ] && return 0
        sleep 0.2
    done
    return 1
}



################################################
# start/stop
################################################
stop_mark=$data_dir/stop
function stop_auto_print {
    echo_info "Trying to stop script..."
    touch $stop_mark
    wait_for_script_lock_release 30; wait_exit_code=$?
    if [ $wait_exit_code -eq 0 ]
    then
        echo; echo_info "Stop ok."; return 0
    else
        echo; echo_error "Stop failed O.o"; return 1
    fi
}
function start_auto_print {
    echo_info "Trying to start script..."
    rm -v $stop_mark
    wait_for_script_lock 15; wait_exit_code=$?
    if [ $wait_exit_code -eq 0 ]
    then
        echo; echo_info "Start ok."; return 0
    else
        echo; echo_error "start failed O.o"; return 1
    fi
}
function restart_auto_print {
    echo_info "Trying to restart script..."
    stop_auto_print && start_auto_print && echo_info "Restart ok." && return 0
    echo_error "Restart failed." && return 1
}
function i_must_stop {
    [ -f $stop_mark ] && return 0 || return 1
}



################################################
# printer functions
################################################
function printer_to_dir {
    printer=$1
    echo "/srv/$printer"
}
function printer_to_printed_dir {
    printer=$1
    printer_dir=$(printer_to_dir $printer)
    echo "$printer_dir/printed"
}
function clean_printer_dir {
    printer=$1
    printer_dir=$(printer_to_dir $printer)
    echo_debug "cleaning last_errors older than [$last_error_expiration_days] days in [$printer_dir]..."
    find $printer_dir -maxdepth 1 -type f -name "*.last_error" -mtime +$last_error_expiration_days -exec rm -v {} \;
}
function clean_printed_dir {
    printer=$1
    printed_dir=$(printer_to_printed_dir $printer)
    [ -d $printed_dir ] || return 0
    echo_debug "cleaning printed txt older than [$printed_txt_expiration_days] days in [$printed_dir]..."
    find $printed_dir -maxdepth 1 -type f -name "*.txt" -mtime +$printed_txt_expiration_days -exec rm -v {} \;
}
function printer_jobs {
    printer=$1
    lpstat -R | egrep "$printer( |-)"
}
function printer_jobs_count {
    printer=$1
    printer_jobs $printer | wc -l
}
function printer_raw_status {
    printer=$1
    lpstat -p $printer
}
function printer_raw_hardware {
    printer=$1
    lpstat -v $printer
}
socket_pattern='socket://[0-9]*.[0-9]*.[0-9]*.[0-9]*'
function is_a_network_printer {
    printer=$1
    raw_hardware=$(printer_raw_hardware $printer)
    echo $raw_hardware | grep $socket_pattern > /dev/null && return 0 || return 1
}
function printer_web_adress {
    printer=$1
    is_a_network_printer $printer || return 1
    web_adress=$(echo $raw_hardware | egrep --only-matching $socket_pattern | sed 's,socket,http,' )
    echo $web_adress; return 0
}
function printer_online_web_adress {
    printer=$1
    is_a_network_printer $printer || return 1
    web_adress=$(printer_web_adress $printer)
    curl --max-time 3 $web_adress > /dev/null 2>&1 || return 2
    echo $web_adress; return 0
}
function web_interface_up {
    printer=$1
    web_adress=$(printer_online_web_adress $printer); acces_exit_code=$?
    if [ $acces_exit_code -eq 0 ]
    then
        return 0
    else
        echo_warning "printer [$printer] web interface [$(printer_web_adress $printer)] unavailable."
        return 1
    fi
}
function update_printer_status {
    printer=$1
    printer_dir=$(printer_to_dir $printer)
    printer_status=$printer_dir/printer_status.log
    new_printer_status=$data_dir/new_printer_status.$printer.tmp
    date -Isecond > $new_printer_status
    echo >> $new_printer_status
    echo >> $new_printer_status
    echo "==== [$printer] hardware ====" >> $new_printer_status
    printer_raw_hardware $printer >> $new_printer_status 2>&1
    echo >> $new_printer_status
    echo >> $new_printer_status
    echo "==== [$printer] status ====" >> $new_printer_status
    printer_raw_status $printer >> $new_printer_status 2>&1
    web_adress=$(printer_online_web_adress $printer); web_adress_exit_code=$?
    if [ $web_adress_exit_code -eq 0 ]
    then
        echo "-----" >> $new_printer_status
        echo "You might find additional information at [$web_adress]." >> $new_printer_status
    fi
    echo >> $new_printer_status
    echo >> $new_printer_status
    echo "==== [$printer] queue state ====" >> $new_printer_status
    lpstat -a $printer >> $new_printer_status 2>&1
    echo >> $new_printer_status
    echo >> $new_printer_status
    echo "==== CUPS service status ====" >> $new_printer_status
    lpstat -r >> $new_printer_status 2>&1
    echo >> $new_printer_status
    echo >> $new_printer_status
    echo "==== [$printer] print jobs ($(printer_jobs_count $printer)) ====" >> $new_printer_status
    printer_jobs $printer >> $new_printer_status 2>&1
    echo >> $new_printer_status
    echo >> $new_printer_status
    echo "==== All print jobs ====" >> $new_printer_status
    lpstat -R >> $new_printer_status 2>&1
    echo >> $new_printer_status
    echo >> $new_printer_status
    cat $new_printer_status > $printer_status
    rm $new_printer_status
}
function printer_not_installed {
    printer=$1
    printer_raw_status $printer 2>&1 | grep "Invalid destination name" > /dev/null && return 0 || return 1
}
function printer_unavailable {
    printer=$1
    printer_raw_status $printer 2>&1 | grep -i "Waiting for printer to become available" > /dev/null && return 0
    is_a_network_printer $printer && ! web_interface_up $printer && return 0
    return 1
}
function scheduler_is_running {
    lpstat -r 2>&1 | grep -i "scheduler is running" > /dev/null && return 0 || return 1
}
function printer_blocked {
    printer=$1
    scheduler_is_running || return 1
    printer_raw_status $printer 2>&1 | grep -i "Rendering completed" > /dev/null && return 0
    jobs_count=$(printer_jobs_count $printer)
    [ $jobs_count -gt 0 ] && return 0
    web_adress=$(printer_online_web_adress $printer); web_adress_exit_code=$?
    if [ $web_adress_exit_code -eq 0 ]
    then
        curl --max-time 3 $web_adress 2> /dev/null | grep -i "Status:" | grep -i "PAUSED" > /dev/null && return 0
    fi
    return 1
}
function printer_is_probably_ready {
    printer=$1
    if printer_not_installed $printer
    then
        echo_warning "printer [$printer] is unknown. Please install the printer [$printer] on this system first."
        return 1
    fi
    if printer_unavailable $printer
    then
        echo_warning "printer [$printer] is NOT ready to print (unavailable). Maybe offline ? Please, check connectivity."
        return 1
    fi
    if printer_blocked $printer
    then
        echo_warning "printer [$printer] is blocked. Maybe paper or ink problem ? Please, check the printer itself."
        return 1
    fi
    echo_debug "printer [$printer] is (probably) ready to print"
    return 0
}
function try_to_print {
    file_path_to_print="$1"
    printer=$2
    echo_debug "trying to print [$file_path_to_print] on [$printer]..."
    last_error=$(echo $file_path_to_print | sed 's,\.txt$,.last_error,')
    echo_debug "last_error[$last_error]"
    printed_dir=$(printer_to_printed_dir $printer)
    printed=$(echo $file_path_to_print | sed "s,^$(printer_to_dir $printer),$printed_dir,")
    printed=$(echo $printed | sed 's,\.sent_to_printer_at_..............\.txt$,.txt,')
    printed=$(echo $printed | sed "s,\.txt\$,.sent_to_printer_at_$(date +%Y%m%d%H%M%S).txt,")
    echo_debug "printed[$printed]"
    date -Isecond > "$last_error"
    echo >> "$last_error"
    echo >> "$last_error"
    echo "==== printer pre-check ====" >> "$last_error"
    printer_is_probably_ready $printer > "$last_error" 2>&1 || return 1
    echo >> "$last_error"
    echo >> "$last_error"
    echo "==== try printing... ====" >> "$last_error"
    md5_1=$(md5sum "$file_path_to_print")
    sleep 0.2
    md5_2=$(md5sum "$file_path_to_print")
    if [ "$md5_1" != "$md5_2" ]
    then
        echo_debug "md5 of file [$file_path_to_print] changed from [$md5_1] to [$md5_2]. Aborting print"
        echo "File content still being modified. Abording print for now." >> "$last_error"
        return 1
    fi
    lp -d $printer "$file_path_to_print" >> "$last_error" 2>&1 || return 1
    sleep 5
    [ -f $printed_dir ] && rm -Rfv $printed_dir
    mkdir -p $printed_dir >> "$last_error" 2>&1 || return 1
    chown root:users $printed_dir >> "$last_error" 2>&1 || return 1
    chmod 770 $printed_dir >> "$last_error" 2>&1 || return 1
    rm -v "$last_error"
    mv -v "$file_path_to_print" "$printed" || return 1
    return 0
}
