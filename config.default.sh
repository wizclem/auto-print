# DONT EDIT
# instead, create a file named config.custom.sh and overwrite these variables into it.

print_debug=false
script_lock_expiration_minutes=1
script_max_life_sec=$((5*60))
printed_txt_expiration_days=7
last_error_expiration_days=7
