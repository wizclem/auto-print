#!/bin/bash

source `dirname $0`"/includes.sh" || exit 99

if i_must_stop && ! $onerun
then
    echo_debug "auto-print stopped. (restart it via custom webmin command [restart auto-print])"
    exit 98
fi
take_script_lock_or_exit



echo_info "==== START ===="
script_start_time=$(date +%s)
while [ $(date +%s) -lt $(($script_start_time + $script_max_life_sec)) ]
do
    refresh_script_lock_or_exit
    i_must_stop && ! $onerun && break
    for printer_num in {1..100}
    do
        printer="printer"$printer_num
        printer_dir=$(printer_to_dir $printer)
        [ -d $printer_dir ] || continue
        echo_info "=== $printer ==="
        clean_printer_dir $printer
        clean_printed_dir $printer
        update_printer_status $printer
        printer_is_probably_ready $printer
        file_path_to_print="$(find $printer_dir -type f -name '*.txt' -maxdepth 1 | sort | head -1)"
        [ -f "$file_path_to_print" ] && try_to_print "$file_path_to_print" $printer
        refresh_script_lock_or_exit
    done # for printer_num
    $onerun && break
    sleep 2
done



release_script_lock
echo_info "==== END ===="
exit 0
